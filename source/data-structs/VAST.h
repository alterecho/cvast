//VAST.h

#include "VASTError.h"
#include "VASTAd.h"

struct VAST {
	VASTError 	*error;
	VASTAd		*ads[];
}

typedef struct VAST VAST;
